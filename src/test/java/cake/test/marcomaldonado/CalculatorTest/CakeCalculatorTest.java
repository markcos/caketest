package cake.test.marcomaldonado.CalculatorTest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.junit.Before;
import org.testng.Assert;

public class CakeCalculatorTest extends CakeTest {

	public CakeCalculatorTest(String url) {
		super(url);
	}

	private CakeCalculatorPage objCalculatorPage;
	public final static String  TESTURL="http://eddyjones.com/test_stuff/advanced.html";
	

	public CakeCalculatorTest() {

		super(TESTURL);
		objCalculatorPage = new CakeCalculatorPage(this.getDriver());
	}
	
	@org.testng.annotations.BeforeMethod
	public void BeforeMethod() {
		
		this.getDriver().navigate().refresh();
	}

	@Test
	public void ValidIntegerAdditionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SumtwoNumbers(2, 2).equals("4"),
				"ValidIntegerAdditionTest - Addition operation failed");
	}

	//NOTE bug.. No decimal?
	@Test
	public void InvalidDoubleAdditionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SumtwoNumbers(2.5, 2.3).equals("4"),
				"InvalidDoubleAdditionTest - Addition operation failed");
	}

	@Test
	public void CharsAdditionTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SumtwoNumbers("rr", "rdhh").equals("NaN"),
				"CharsAdditionTest - Addition operation failed");
	}

	@Test
	public void EmptySpacesTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SumtwoNumbers(" ", " ").equals("NaN"),
				" EmptySpacesTest- Addition operation failed");
	}

	@Test
	public void SignIntegerAdditionTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SumtwoNumbers("-1", "-1").equals("-2"),
				" SignIntegerAdditionTest- Addition operation failed");

	}

	@Test
	public void LongIntegerAdditionTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SumtwoNumbers(10000001, 20000001).equals("30000002"),
				"LongIntegerAdditionTest - Addition operation failed");
	}

	// NOTE: this scenario is a bug. It does not Throw NaN. INstead does the
	// operation and omits the characters
	@Test
	public void AddCharAfterFirstNumberTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SumtwoNumbers("5r", "5d").equals("NaN"),
				" AddCharAfterFirstNumberTest- Addition operation failed");
	}

	// NOTE: sum Bug!- first number we are adding 1 to impairs.. second number
	// adding 1 to pairs
	@Test
	public void pairImpairAdditionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SumtwoNumbers(1, 4).equals("5"),
				"pairImpairAdditionTest - Addition operation failed");
	}

	@Test
	public void ValidIntegerSubstractionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SubtwoNumbers(-2, -2).equals("0"),
				"ValidIntegerSubstractionTest - Substraction operation failed");
	}

	 //Note bug.. no decimal??
	@Test
	public void InvalidDoubleSubstractionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SubtwoNumbers(2.5, 2.3).equals("0"),
				"InvalidDoubleSubstractionTest - Substraction operation failed");
	}

	@Test
	public void CharsSubstractionTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SubtwoNumbers("rr", "rdhh").equals("NaN"),
				"CharsSubstractionTest - Substraction operation failed");
	}

	@Test
	public void EmptySpacesSubstractionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SubtwoNumbers(" ", " ").equals("NaN"),
				" EmptySpacesSubstractionTest- Substraction operation failed");
	}


	@Test
	public void LongIntegerSubstractionTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SubtwoNumbers(20000001, 10000001).equals("10000000"),
				"LongIntegerSubstractionTest - Substraction operation failed");

	}

	// NOTE: this scenario is a bug. It does not Throw NaN.
	@Test
	public void AddCharAfterFirstNumberSubstractionTest() throws Exception {
		Assert.assertTrue(objCalculatorPage.SubtwoNumbers("5r", "5d").equals("NaN"),
				" AddCharAfterFirstNumberSubstractionTest- Substraction operation failed");

	}
	// NOTE bug- we are substracting 1 when second number is impair

	@Test
	public void impairSecondNumSubstractionTest() throws Exception {

		Assert.assertTrue(objCalculatorPage.SubtwoNumbers(2, -1).equals("1"),
				"impairSecondNumSubstractionTest - Substraction operation failed");
	}
}
