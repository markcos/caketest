package cake.test.marcomaldonado.CalculatorTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CakeTest
{
	private WebDriver driver;
	
   
    public CakeTest( String url )
    { 	
    	System.setProperty("webdriver.gecko.driver", "C:\\dev\\geckodriver.exe");
    	driver =new FirefoxDriver();
    	driver.get(url);
    }
 
	WebDriver getDriver(){
		return driver;
	}
}
