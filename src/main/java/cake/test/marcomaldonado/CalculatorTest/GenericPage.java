package cake.test.marcomaldonado.CalculatorTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenericPage {

	private WebDriver driver;
	
	protected GenericPage(WebDriver driver){
	
		this.driver=driver;
		
	}
	
    public void waitUntilElementVisible(WebElement element) {

        final FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(10,
                TimeUnit.SECONDS)
                .ignoring(RuntimeException.class).pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOf(element));

    }
    
    public void waitUntilGetText(final WebElement element) {
    
    	(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
    	    public Boolean apply(WebDriver d) {
    	        return String.valueOf(element.getAttribute("value")).length() != 0;
    	    }
    	});

    }
}
