package cake.test.marcomaldonado.CalculatorTest;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CakeCalculatorPage extends GenericPage {

	public CakeCalculatorPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// First Addition Number
	@FindBy(id = "addition_number_1")
	WebElement additionFirstNumberText;
	// second Addition Number
	@FindBy(id = "addition_number_2")
	WebElement additionSecondNumberText;
	// Addition Action button
	@FindBy(id = "addition_action_1")
	WebElement additionActionButton;
	// Addition Result Text
	@FindBy(id = "addition_sum")
	WebElement additionResultText;

	// First subtraction Number
	@FindBy(id = "subtraction_number_1")
	WebElement subtractionFirstNumberText;
	// second subtraction Number
	@FindBy(id = "subtraction_number_2")
	WebElement subtractionSecondNumberText;
	// subtraction Action button
	@FindBy(id = "subtraction_action_1")
	WebElement subtractionActionButton;
	// subtraction Result Text
	@FindBy(id = "subtraction_difference")
	WebElement subtractionResultText;

	@FindBy(css = "sbody h1")
	WebElement pageTitle;

	// Find several elements
	@FindBy(css = "body h2")
	List<WebElement> menuEntries;

	public String SumtwoNumbers(int number1, int number2) {

		waitUntilElementVisible(additionFirstNumberText);
		additionFirstNumberText.sendKeys(String.valueOf(number1));
		additionSecondNumberText.sendKeys(String.valueOf(number2));
		additionActionButton.click();
		waitUntilGetText(additionResultText);
		return (additionResultText.getAttribute("value"));

	}

	public String SubtwoNumbers(double d, double e) {

		waitUntilElementVisible(additionFirstNumberText);
		additionFirstNumberText.sendKeys(String.valueOf(d));
		additionSecondNumberText.sendKeys(String.valueOf(e));
		additionActionButton.click();
		waitUntilGetText(additionResultText);
		return (additionResultText.getAttribute("value"));

	}

	public String SumtwoNumbers(double number1, double number2) {

		waitUntilElementVisible(additionFirstNumberText);
		additionFirstNumberText.sendKeys(String.valueOf(number1));
		additionSecondNumberText.sendKeys(String.valueOf(number2));
		additionActionButton.click();
		return (additionResultText.getAttribute("value"));
	}

	public String SubtwoNumbers(int number1, int number2) {

		waitUntilElementVisible(subtractionFirstNumberText);
		subtractionFirstNumberText.sendKeys(String.valueOf(number1));
		subtractionSecondNumberText.sendKeys(String.valueOf(number2));
		subtractionActionButton.click();
		return (subtractionResultText.getAttribute("value"));
	}

	public String SubtwoNumbers(String number1, String number2) {

		waitUntilElementVisible(subtractionFirstNumberText);
		subtractionFirstNumberText.sendKeys(number1);
		subtractionSecondNumberText.sendKeys(number2);
		subtractionActionButton.click();
		return (subtractionResultText.getAttribute("value"));
	}

	public String SumtwoNumbers(String number1, String number2) {

		waitUntilElementVisible(additionFirstNumberText);
		additionFirstNumberText.sendKeys(number1);
		additionSecondNumberText.sendKeys(number2);
		additionActionButton.click();
		return (additionResultText.getAttribute("value"));
	}

}
